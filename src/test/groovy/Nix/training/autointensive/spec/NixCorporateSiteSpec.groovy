package Nix.training.autointensive.spec

import Nix.training.autointensive.page.LandingPage
import Nix.training.autointensive.page.ProductsPage
import geb.spock.GebReportingSpec

class NixCorporateSiteSpec extends GebReportingSpec{
    def "Navigate to main page"(){
        when:
            to LandingPage

        and:
            "User navigates to Products page"()

        then:
            at ProductsPage

    }
}
