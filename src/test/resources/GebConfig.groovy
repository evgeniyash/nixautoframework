import org.openqa.selenium.chrome.ChromeDriver

    System.setProperty("geb.build.reportsDir", "target/geb-reports")
    System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com")
    System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver 2")
    System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe")
    System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com")

    if(System.properties['os.name'].toLowerCase().contains('windows')){
        System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver1.exe")
    }
    else{
        System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver 2")
    }

    driver = {
        def driver = new ChromeDriver()
        driver.manage().window().maximize()
        return driver
    }